#!/bin/bash

rust_toolchain=$(cat ${TONE_BM_RUN_DIR}/image-service/rust-toolchain)
compile_image="localhost/compile-image:${rust_toolchain}"
local_registry_port="5000"


start_local_docker_registry()
{
    if docker ps | grep myregistry | grep $local_registry_port; then
        echo "local registry container is running"
    else
        docker run -d --restart=always -p $local_registry_port:$local_registry_port -v /myregistry:/var/lib/registry --name myregistry registry
        if [ $? -ne 0 ]; then
            echo "fail to start registry container"
            exit 1
        fi
    fi
}

build_docker_image()
{
    dockerfile="/tmp/rust_golang_dockerfile"
	cat > $dockerfile <<EOF
FROM rust:${rust_toolchain}
RUN apt-get update -y \
    && apt-get install -y cmake g++ pkg-config jq libcurl4-openssl-dev libelf-dev libdw-dev binutils-dev libiberty-dev musl-tools \
    && rustup component add rustfmt clippy \
    && rm -rf /var/lib/apt/lists/*
# install golang env
Run wget https://go.dev/dl/go1.19.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.19.linux-amd64.tar.gz \
    && rm -rf go1.19.linux-amd64.tar.gz
ENV PATH \$PATH:/usr/local/go/bin
RUN go env -w GO111MODULE=on
RUN go env -w GOPROXY=https://goproxy.io,direct
EOF
    yum install -y docker
    docker build -f $dockerfile -t $compile_image .
    if [ $? -ne 0 ]; then
        echo "fail to build docker image"
        exit 1
    fi
}

compile_nydusd()
{
    docker run --rm -v ${TONE_BM_RUN_DIR}/image-service:/image-service $compile_image bash -c 'cd /image-service && make clean && make release'
    if [ -f "${TONE_BM_RUN_DIR}/image-service/target/release/nydusd" ] && [ -f "${TONE_BM_RUN_DIR}/image-service/target/release/nydus-image" ]; then
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/image-service/target/release/nydusd /usr/local/bin/
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/image-service/target/release/nydus-image /usr/local/bin/
    else
        echo "cannot find nydusd binary or nydus-image binary"
        exit 1
    fi
}

compile_nydus_snapshotter()
{
    rm -rf /var/lib/containerd/io.containerd.snapshotter.v1.nydus
    rm -rf /var/lib/nydus/cache
    docker run --rm -v ${TONE_BM_RUN_DIR}/nydus-snapshotter:/nydus-snapshotter $compile_image bash -c 'cd /nydus-snapshotter && make clean && make'
    if [ -f "${TONE_BM_RUN_DIR}/nydus-snapshotter/bin/containerd-nydus-grpc" ]; then
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/nydus-snapshotter/bin/containerd-nydus-grpc /usr/local/bin/
    else
        echo "cannot find containerd-nydus-grpc binary"
        exit 1
    fi
}

run_nydus_snapshotter()
{
    rm -rf /var/lib/containerd/io.containerd.snapshotter.v1.nydus
    rm -rf /var/lib/nydus/cache
    cat >/tmp/nydus-erofs-config.json <<EOF
{
  "type": "bootstrap",
  "config": {
    "backend_type": "registry",
    "backend_config": {
      "scheme": "https"
    },
    "cache_type": "fscache"
  }
}
EOF
    containerd-nydus-grpc --config-path /tmp/nydus-erofs-config.json --daemon-mode shared \
        --fs-driver fscache --root /var/lib/containerd/io.containerd.snapshotter.v1.nydus \
        --address /run/containerd/containerd-nydus-grpc.sock --nydusd /usr/local/bin/nydusd \
        --log-to-stdout >${TONE_BM_RUN_DIR}/nydus-snapshotter.log 2>&1 &
}

config_containerd_for_nydus() 
{
    [ -d "/etc/containerd" ] || mkdir -p /etc/containerd
    cat >/etc/containerd/config.toml <<EOF
version = 2
[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    [plugins."io.containerd.grpc.v1.cri".cni]
      bin_dir = "/usr/lib/cni"
      conf_dir = "/etc/cni/net.d"
  [plugins."io.containerd.internal.v1.opt"]
    path = "/var/lib/containerd/opt"
[proxy_plugins]
  [proxy_plugins.nydus]
    type = "snapshot"
    address = "/run/containerd/containerd-nydus-grpc.sock"
[plugins."io.containerd.grpc.v1.cri".containerd]
   snapshotter = "nydus"
   disable_snapshot_annotations = false
EOF
    systemctl restart containerd
    if [ $? -ne 0 ]; then
        echo "fail to restart containerd service"
        exit 1
    fi
}

run()
{
    start_local_docker_registry
    build_docker_image
    compile_nydusd
    compile_nydus_snapshotter
    run_nydus_snapshotter
    config_containerd_for_nydus

    export round_number=${round_number:-1}
    export image_list=${image_list:-""}
    cd ${TONE_BM_RUN_DIR}/hello-bench
    if [ -z "$image_list" ]; then
        ./run.sh -o push -p ./image_list.txt -t localhost:$local_registry_port
        ./run.sh -o convert -p ./image_list.txt -t localhost:$local_registry_port
        ./run.sh -o run -p ./image_list.txt -t localhost:$local_registry_port -r $round_number
    else
        ./run.sh -o push -i ${image_list} -t localhost:$local_registry_port
        ./run.sh -o convert -i ${image_list} -t localhost:$local_registry_port
        ./run.sh -o run -i ${image_list} -t localhost:$local_registry_port -r $round_number
    fi
}

teardown()
{
    systemctl stop nydus-snapshotter.service || true
    if ps -ef | grep containerd-nydus-grpc | grep -v grep; then
        ps -ef | grep containerd-nydus-grpc | grep -v grep | awk '{print $2}' | xargs kill -9
    fi
    if ps -ef | grep nydusd | grep fscache; then
        ps -ef | grep nydusd | grep fscache | awk '{print $2}' | xargs kill -9
    fi
    if mount | grep 'erofs on'; then
        mount | grep 'erofs on' | awk '{print $3}' | xargs umount
    fi
    /usr/bin/cp ${TONE_BM_RUN_DIR}/*.log $TONE_CURRENT_RESULT_DIR/ || true
    /usr/bin/cp -r ${TONE_BM_RUN_DIR}/hello-bench/data $TONE_CURRENT_RESULT_DIR/ || true
    /usr/bin/cp ${TONE_BM_RUN_DIR}/hello-bench/bench.json $TONE_CURRENT_RESULT_DIR/ || true
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py ${TONE_BM_RUN_DIR}/hello-bench/data
}
