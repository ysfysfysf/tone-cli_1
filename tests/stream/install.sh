#!/bin/bash
GIT_URL="https://github.com/jeffhammond/STREAM.git"

if [[ "ubuntu debian uos kylin" =~  $TONE_OS_DISTRO ]];then
    DEP_PKG_LIST="gfortran"
else
    DEP_PKG_LIST="gcc-gfortran"
fi

fetch()
{
	ls $TONE_BM_SUITE_DIR/STREAM.tar.bz2
}

extract_src()
{
	tar jxvf $TONE_BM_SUITE_DIR/STREAM.tar.bz2 -C $TONE_BM_BUILD_DIR/
	cd $TONE_BM_BUILD_DIR/STREAM
	patch -p1 < $TONE_BM_SUITE_DIR/stream.100M.patch
}

build()
{
	cd $TONE_BM_BUILD_DIR/STREAM
	make
}

install()
{
	cp -af * "$TONE_BM_RUN_DIR"
}
