#!/bin/bash

KERNEL_CI_REPO_URL=$2
KERNEL_CI_REPO_BRANCH=$3
KERNEL_CI_PR_ID=$4
CK_BUILDER_REPO="https://gitee.com/src-anolis-sig/ck-build.git"

if [ $(arch) == "x86_64" ]; then
	kernel_arch="x86"
elif [ $(arch) == "aarch64" ]; then
	kernel_arch="arm64"
else
	echo "Not supported arch"
	exit 1
fi

[ -n "$KERNEL_CI_REPO_URL" ] || {
    echo "Error: KERNEL_CI_REPO_URL is not set"
    exit 1
}
[ -n "$KERNEL_CI_REPO_BRANCH" ] || {
    echo "Error: KERNEL_CI_REPO_BRANCH is not set"
    exit 1
}
[ -n "$KERNEL_CI_PR_ID" ] || {
    echo "Warning: KERNEL_CI_PR_ID is not set"
}

[ -n "$CK_BUILDER_BRANCH" ] || {
    if echo "$KERNEL_CI_REPO_BRANCH" | grep -q "4.19"; then
        CK_BUILDER_BRANCH="an8-4.19"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "5.10"; then
        CK_BUILDER_BRANCH="an8-5.10"
    else
        CK_BUILDER_BRANCH="an8-4.19"
    fi
}


job_num=$(cat /proc/cpuinfo | grep processor | wc | awk {'print $1-1'})

mkdir -p /anck_build
cd /anck_build
anck_repo=$(basename $KERNEL_CI_REPO_URL .git)
[ -d "$anck_repo" ] && {
    echo "$anck_repo exist, try to remove it..."
    rm -rf $anck_repo
}

echo "===> Clone kernel repository..."
git clone --depth 1 -b $KERNEL_CI_REPO_BRANCH $KERNEL_CI_REPO_URL
if [ "X$KERNEL_CI_PR_ID" == "X" ]; then
    echo "===> KERNEL_CI_PR_ID not set, Skip apply patch"
    cd $anck_repo
else
    patch_url="${KERNEL_CI_REPO_URL/\.git/}/pulls/${KERNEL_CI_PR_ID}.patch"
    echo "===> Get the patch from: $patch_url"
    [ -f $(basename $patch_url) ] && rm -f $(basename $patch_url)
    echo "CMD: wget $patch_url"
    wget $patch_url
    cat $(basename $patch_url)
    cd $anck_repo
    echo "===> Apply patch: $patch_url"
    echo "CMD: git config user.email \"test@test.com\""
    echo "CMD: git config user.name \"test\""
    git config user.email "test@test.com"
    git config user.name "test"
    echo "CMD: git am ../$(basename $patch_url)"
    if git am ../$(basename $patch_url); then
    	echo "Apply patch pass: $patch_url"
    else
    	echo "Apply patch fail: $patch_url"
    	exit 1
    fi
    echo "===> Apply patch done"
fi

echo "===> Install related packages..."
echo "CMD: yum install -y glibc-static flex bison elfutils-libelf-devel openssl-devel dwarves"
yum install -y glibc-static flex bison elfutils-libelf-devel openssl-devel dwarves
echo "===> Install packages done"

if [ $1 == "allyesconfig" ]; then
    echo "== Build Kernel with allyesconfig =="
    echo "make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s"
    make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s
    if [ $? -ne 0 ]; then
        echo "anck_allyesconfig: fail"
        exit 1
    else
        echo "anck_allyesconfig: pass"
        exit 0
    fi
elif [ $1 == "defconfig" ]; then
    echo "== Build Kernel with anolis_defconfig =="
    echo "CMD: make clean && cp -f arch/${kernel_arch}/configs/anolis_defconfig .config && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && cp -f arch/${kernel_arch}/configs/anolis_defconfig .config && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    if [ $? -ne 0 ]; then
        echo "anck_anolis_defconfig: fail"
        exit 1
    else
        echo "anck_anolis_defconfig: pass"
        exit 0
    fi
elif [ $1 == "allnoconfig" ]; then
    echo "== Build Kernel with allnoconfig =="
    echo "CMD: make clean && make allnoconfig && make -j $job_num -s"
    make clean && make allnoconfig && make -j $job_num -s
    if [ $? -ne 0 ]; then
        echo "anck_allnoconfig: fail"
        exit 1
    else
        echo "anck_allnoconfig: pass"
        exit 0
    fi
elif [ $1 == "debugconfig" ]; then
    echo "== Build Kernel with anolis-debug_defconfig =="
    echo "CMD: make clean && cp -f arch/${kernel_arch}/configs/anolis-debug_defconfig .config && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && cp -f arch/${kernel_arch}/configs/anolis-debug_defconfig .config && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    if [ $? -ne 0 ]; then
        echo "anck_debug_defconfig: fail"
        exit 1
    else
        echo "anck_debug_defconfig: pass"
        exit 0
    fi
elif [ $1 == "checkconfig" ]; then
    echo "== Check kconfig =="
    check_status=0
    for cfg in $(ls arch/${kernel_arch}/configs/anolis_*);
    do
        echo "Check $cfg"
        echo "CMD: cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions"
        cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions
        [ -s .newoptions ] && cat .newoptions && check_status=1;
    done
    if [[ $check_status -ne 0 ]];then
        echo "anck_check_kconfig: fail"
        exit 1
    else
        echo "anck_check_kconfig: pass"
        exit 0
    fi
elif [ $1 == "rpmbuild" ]; then
    echo "== Build Kernel RPM =="
    echo "Get ck builder..."
    cd /anck_build
    ck_builder=$(basename $CK_BUILDER_REPO .git)
    [ -d "$ck_builder" ] && rm -rf $ck_builder
    git clone -b $CK_BUILDER_BRANCH $CK_BUILDER_REPO || return 1
    [ _"$ck_builder" != "_ck-build" ] && ln -s $ck_builder ck-build
    cd $ck_builder
    # for an8-4.19
    if grep ^srcdir build.sh | grep -q ck.git; then
        ln -sf ../${anck_repo} ck.git
    fi
    # for an8-5.10
    if grep ^srcdir build.sh | grep -q cloud-kernel; then
        ln -sf ../${anck_repo} cloud-kernel
    fi

    [ -d "outputs" ] && rm -rf outputs
    yum install -y yum-utils
    yum-builddep -y kernel.spec
    echo "Build ANCK..."
    [ -n "$BUILD_EXTRA" ] || export BUILD_EXTRA="base"
    sh build.sh
    if [ $? -ne 0 ]; then
        echo "anck_rpmbuild: fail"
        exit 1
    else
        echo "anck_rpmbuild: pass"
    fi
    exit 0
fi