#!/bin/bash

ltp_blacklist=$TONE_BM_SUITE_DIR/ltp.blacklist
sctp_flag=0

get_kernel_info()
{
	kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
	os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')
	arch=$(uname -m)
}

get_kernel_info

prepare_for_blacklist()
{
	# tpci testcase run on ecs will let the machine hanged, skip it
	systemd-detect-virt --vm -q && grep -q "^tpci$" $ltp_blacklist || echo "tpci" >>$ltp_blacklist
        
	# https://bugzilla.openanolis.cn/show_bug.cgi?id=2213
	if [ x"$os_ver" == x"an7" ]; then
		an7_skip_list=$(grep -v '^#' $TONE_BM_SUITE_DIR/ltp.an7.blacklist)
		for an7_case in $an7_skip_list
		do
			grep -q "^$an7_case$" $ltp_blacklist || echo "$an7_case" >>$ltp_blacklist
		done
	fi

	if [ x"$kernel_ver" == x"4.19" -a x"$arch" == x"aarch64" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=2202
		grep -q "^pty04$" $ltp_blacklist || echo "pty04" >>$ltp_blacklist
		grep -q "^cve-2020-11494$" $ltp_blacklist || echo "cve-2020-11494" >>$ltp_blacklist
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=2262
		grep -q "^irqbalance01$" $ltp_blacklist || echo "irqbalance01" >>$ltp_blacklist
	fi

	if [ x"$os_ver" == x"an8" -a x"$kernel_ver" == x"5.10" -a  x"$arch" == x"x86_64" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=2213
		grep -q "^macsec02$" $ltp_blacklist || echo "macsec02" >>$ltp_blacklist
		grep -q "^macsec03$" $ltp_blacklist || echo "macsec03" >>$ltp_blacklist
	fi

	if [ x"$arch" == x"aarch64" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=1762
		grep -q "^test_1_to_1_events" $ltp_blacklist || echo "test_1_to_1_events" >>$ltp_blacklist
	fi

	if [ x"$os_ver" == x"an7" -a x"$kernel_ver" == x"4.19" -a  x"$arch" == x"x86_64" ]; then
		# Probabilistic failure testcases, temporary skip
		grep -q "^busy_poll01$" $ltp_blacklist || echo "busy_poll01" >>$ltp_blacklist
		grep -q "^busy_poll02$" $ltp_blacklist || echo "busy_poll02" >>$ltp_blacklist
	fi

	if [ x"$kernel_ver" == x"4.19" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=3070
		grep -q "^route6-change-netlink-dst" $ltp_blacklist || echo "route6-change-netlink-dst" >>$ltp_blacklist
		grep -q "^route6-change-netlink-gw" $ltp_blacklist || echo "route6-change-netlink-gw" >>$ltp_blacklist
		grep -q "^route6-change-netlink-if" $ltp_blacklist || echo "route6-change-netlink-if" >>$ltp_blacklist
	fi

	if [ x"$os_ver" == x"an23" -a x"$kernel_ver" == x"5.10" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=4804
		grep -q "^oom01" $ltp_blacklist || echo "oom01" >>$ltp_blacklist
		grep -q "^min_free_kbytes" $ltp_blacklist || echo "min_free_kbytes" >>$ltp_blacklist
	fi
}

setup_for_net()
{	
	if [ "$group" == "net.sctp" ]; then
		lsmod | grep -q sctp || modprobe sctp && sctp_flag=1	
	fi
}

cleanup_for_net()
{	
	if [ "$group" == "net.sctp" -a $sctp_flag == 1 ]; then
		modprobe -r sctp
	fi
}

prepare_for_param()
{
	ltp_param=''
	[ -z $LTP_TMPDIR ] || ltp_param=${ltp_param}" -d $LTP_TMPDIR"
	[ -z $LTP_DEV ] || ltp_param=${ltp_param}" -b $LTP_DEV"
	[ -z $LTP_DEV_FS ] || ltp_param=${ltp_param}" -B $LTP_DEV_FS"
	[ -z $LTP_BIG_DEV ] || ltp_param=${ltp_param}" -z $LTP_BIG_DEV"
	[ -z $LTP_BIG_DEV_FS ] || ltp_param=${ltp_param}" -Z $LTP_BIG_DEV_FS"
}
setup()
{
	echo 1    > /proc/sys/kernel/panic
	echo 1    > /proc/sys/kernel/softlockup_panic
	echo 50   > /proc/sys/kernel/watchdog_thresh
	echo 1    > /proc/sys/kernel/hung_task_panic
	echo 1200 > /proc/sys/kernel/hung_task_timeout_secs
	systemctl start kdump.service
	prepare_for_blacklist
	setup_for_net
}

run()
{
	prepare_for_param
	if [ "$group" = all ]; then
		logger ./runltp -S $ltp_blacklist $ltp_param
	else
		logger ./runltp -f $group -S $ltp_blacklist $ltp_param
	fi
}

parse()
{
	awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
	cleanup_for_net
	exit 0
}
