#!/bin/bash

TONE_CAN_SKIP_PKG="yes"
DEP_PKG_LIST="python36 python36-devel rust openssl-devel git kernel-headers"


check_kernel_headers()
{
    rpm -qa|grep kernel-headers-$(uname -r) && return 0
    local version=$(yum list --showduplicates  kernel-headers-`uname -r` |tail -1|awk '{print $2}')
    [ -n "$version" ] && yum install -y kernel-headers-$version && return 0 || return 1
}

set_python_env()
{
    pip3 install --upgrade pip
    pip3 install tornado==6.1
    pip3 install numpy==1.19.5
    pip3 install scikit_learn==0.24.2
    pip3 install hyperopt==0.2.5
}

check_rust()
{
    rpm -q rust && return 0
    rpm -q epel-release || yum install -y epel-release
    yum install rust -y && return 0 || return 1
}

set_env()
{
    check_kernel_headers
    check_rust
    set_python_env
}

build()
{
    set_env
}

install()
{
    echo "install server finshed......."
}
