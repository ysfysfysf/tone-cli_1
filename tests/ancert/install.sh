# Avaliable environment:
#
# Download variable:Download variable
GIT_URL="https://gitee.com/anolis/ancert.git"
BM_NAME="ancert"
DEP_PKG_LIST="git make gcc gcc-c++ fio nvme-cli glx-utils python3 rpm-build bc lvm2 alsa-lib alsa-utils virt-what smartmontools hdparm xorg-x11-utils xorg-x11-server-utils xorg-x11-apps"

fetch()
{
    git_clone $GIT_URL $TONE_BM_CACHE_DIR
}

extract_src()
{
    :
}

build()
{
    yum install -y python-futures
    pip3 install pyyaml
}

install()
{
    [ -d "$TONE_BM_RUN_DIR/$BM_NAME" ] && rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    cp -af $TONE_BM_CACHE_DIR $TONE_BM_RUN_DIR
}

uninstall()
{
    rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    rm -rf $TONE_BM_CACHE_DIR
}
