#GIT_URL="https://github.com/glmark2/glmark2.git"
GIT_URL="https://gitee.com/cn-loongson/glmark2.git"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST="g++ build-essential pkg-config libx11-dev libgl1-mesa-dev libjpeg-dev libpng-dev"
else
	DEP_PKG_LIST="automake gcc gcc-c++ kernel-devel pkg-config libX11-devel mesa-libGL-devel libjpeg-devel libpng-devel"
fi

build()
{
	# remove system version fio
    if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
        dpkg -l | grep -q glmark2 && dpkg -r glmark2
    fi

    ./waf configure --with-flavors=x11-gl
    ./waf build -j 4
    
}

install()
{
	  ./waf install
    strip -s /usr/local/bin/glmark2
}
