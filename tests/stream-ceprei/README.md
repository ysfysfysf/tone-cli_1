# stream
## Description
#The STREAM benchmark is a simple synthetic benchmark program that measures sustainable memory bandwidth (in MB/s) and the corresponding computation rate for simple vector kernels.

## Homepage
http://www.cs.virginia.edu/stream/FTP/Code/stream.c

## Version
5.10

## Category
performance

## Parameters
- crossnode: 是否跨节点测试
- nr_task:	 线程数目

## Results
```
element_size_byte: 8.00
array_size: 100000000.00
Copy: 53725.30
Scale: 53590.60
Add: 55903.90
Triad: 55984.70

```
