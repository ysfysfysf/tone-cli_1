# sysbench
## Description
sysbench is a scriptable multi-threaded benchmark tool based on
LuaJIT. It is most frequently used for database benchmarks, but can also
be used to create arbitrarily complex workloads that do not involve a
database server.

sysbench comes with the following bundled benchmarks:

- `oltp_*.lua`: a collection of OLTP-like database benchmarks
- `fileio`: a filesystem-level benchmark
- `cpu`: a simple CPU benchmark
- `memory`: a memory access benchmark
- `threads`: a thread-based scheduler benchmark
- `mutex`: a POSIX mutex benchmark

## Homepage
https://github.com/akopytov/sysbench

## Version
1.1.0-e4e6d67

## Category
performance

## Parameters
- nr_task: number of threads to use.
- testname: compiled-in tests(cpu/memory/threads/mutex).

## Results
Throughput: 11978.42 eps
workload: 119804.00
latency_min: 1.70 ms
latency_avg: 2.00 ms
latency_max: 13.90 ms
latency_95th: 2.03 ms
thread_events_avg: 4991.83
thread_events_stddev: 27.89
exec_time_avg: 9.9985 s
exec_time_stddev: 0.00

## Manual Run
