#!/bin/bash
# Avaliable environment variables:
# $USERNAME: host username, only one or username list seperated by ','
# $PASSWROD: host password, only one or password list seperated by ','
# $HOST_IP: host ip list seperated by ','
# $HOST_NAME: host name list seperated by ','
# $TESTCASES: testcase name, specify one or more testcases to run, seperated by ','
# $PACKAGE: package name, specify one or more PACKAGE to run, seperated by ','
. $TONE_ROOT/lib/testinfo.sh
setup()
{
    export PYTHONPATH=$PYTHONPATH:$TONE_BM_RUN_DIR/tests
    IFS=","
}
run() 
{
    local case_url
    [ -n "$USERNAME" ] && generate_yaml
    cd $TONE_BM_RUN_DIR
	[ -n "$group" ] || {
        echo "No group is specified"
        exit 1
    }
	if [ "$group" == "all" ]; then
        if [ -n "$TESTCASES" ]; then
            cd $TONE_BM_RUN_DIR
            for loop in ${TESTCASES[@]}; do
                case_url=$(find tests/ -name $loop)
                if  grep 'LocalTest' $case_url ; then
                    avocado run $case_url
                else
                    avocado run $case_url -m hosts.yaml
                fi
            done
        elif [ -n "$PACKAGE_NAME" ]; then
            for loop in ${PACKAGE_NAME[@]}; do
                case_url=$(find tests/ -name $loop)
                package_run $case_url
            done
        else
            getAlldir $TONE_BM_RUN_DIR/tests
            for line in $(cat $TONE_BM_RUN_DIR/run.list); do
                package_run $line
            done
            rm -rf $TONE_BM_RUN_DIR/run.list
        fi
    else
        getAlldir $TONE_BM_RUN_DIR/tests/$group
        for line in $(cat $TONE_BM_RUN_DIR/run.list); do
            package_run $line
        done
        rm -rf $TONE_BM_RUN_DIR/run.list
    fi
}

parse() 
{
    $TONE_BM_SUITE_DIR/parse.py
    
}
teardown() {
    echo "do teardown"
    [ -n "$USERNAME" ] && rm -rf hosts.yaml
}
parse_tag()
{
    local tag_str
    IFS=$','
    for loop in ${TAG[@]}; do
        if [ "$loop" != "remote" ] && [ "$loop" != "local" ]; then
            if [ "$tag_str" == "" ];then
                tag_str=${loop}
            else
                tag_str="${tag_str},${loop}"
            fi
        fi
    done
    echo $tag_str
}
package_run()
{
    local is_remote is_local line tag_str
    line=$1
    if [ -n "$TAG" ]; then
        if [ $(echo $TAG |grep "local"|grep "remote") ]; then
            tag_str=`parse_tag`
            avocado run  --nrunner-max-parallel-tasks 1 -t "local,$tag_str" $line
            avocado run  --nrunner-max-parallel-tasks 1 -t "remote,$tag_str" $line -m $TONE_BM_RUN_DIR/hosts.yaml
        elif [ $(echo $TAG | grep "local") ]; then
            avocado run  --nrunner-max-parallel-tasks 1 -t $TAG $line
        elif [ $(echo $TAG |grep "remote") ]; then
            avocado run  --nrunner-max-parallel-tasks 1 -t $TAG $line -m $TONE_BM_RUN_DIR/hosts.yaml
        else
            is_remote=$(avocado list $line -t remote)
            if [ -n "$is_remote" ]; then
                avocado run  --nrunner-max-parallel-tasks 1 -t "remote,$TAG" $line -m $TONE_BM_RUN_DIR/hosts.yaml
            fi
            is_local=$(avocado list $line -t local)
            if [ -n "$is_local" ]; then
                avocado run  --nrunner-max-parallel-tasks 1 -t "local,$TAG" $line
            fi
        fi
    else
        is_remote=$(avocado list $line -t remote)
        if [ -n "$is_remote" ]; then
            avocado run  --nrunner-max-parallel-tasks 1 -t remote $line -m $TONE_BM_RUN_DIR/hosts.yaml
        fi
        is_local=$(avocado list $line -t local)
        if [ -n "$is_local" ]; then
            avocado run  --nrunner-max-parallel-tasks 1 -t local $line
        fi
    fi
}
generate_yaml() 
{
    username=($(echo $USERNAME | tr '/' ' '))
    password=($(echo $PASSWORD | tr '/' ' '))
    remote_ip=($(echo $REMOTE_IP | tr ',' ' '))
    cat >hosts.yaml <<EOF
username: $username
password: $password
remote: !mux
EOF
    for ((i = 0; i < ${#remote_ip[@]}; i++)); do
        cat >>hosts.yaml <<EOF
    remote_host$(($i+1)):
        remote: ${remote_ip[$i]}
EOF
    done
}
getAlldir()
{
    local dir_tmep fileList fileName
    dir_tmep=$1
    IFS=$'\n'
    fileList=`ls -d $1/*`
    for fileName in $fileList;
    do
        if test -f $fileName; then
            echo $dir_tmep >> $TONE_BM_RUN_DIR/run.list
            return 0
        elif test -d $fileName; then
            result=$(echo $fileName | grep -E "pycache|common")
            if [[ "$result" != "" ]]; then
                continue
            fi
            cd $fileName
            dir_tmep=`pwd`
            getAlldir $fileName
        else
            echo "$fileName is a invalid path"
            return 1
        fi
    done
}
