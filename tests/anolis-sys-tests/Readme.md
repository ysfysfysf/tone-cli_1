# packages-functional-test
## Description

Anolis sys tests

## Version
v0.1

## Category

functional


## Results

```
at_install_check: Pass
at: Pass
at_service_check: Pass
```

## Manual Run

```
pip install -r requirements.txt
avocado -V --show all run packages-functional-test/tests
```

