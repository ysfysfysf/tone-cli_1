#!/bin/bash

GIT_URL=${PKG_CI_REPO_URL:-"https://github.com/aliyun/plugsched"}
BRANCH=${PKG_CI_REPO_BRANCH:-"master"}

fetch()
{
	cd $TONE_BM_CACHE_DIR
	export RETRY_DELAY=30
	retry 10 "rm -rf plugsched && timeout 300 git clone --mirror $GIT_URL"
}

extract_src()
{
	:
}

build()
{
	:
}

install()
{
	cd $TONE_BM_RUN_DIR
	rm -rf plugsched
	git clone -b $BRANCH $TONE_BM_CACHE_DIR/plugsched.git
}
