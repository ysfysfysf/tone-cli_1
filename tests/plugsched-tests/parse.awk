#!/usr/bin/awk -f

/====>>.* test<<====/ {
	match($0, "[a-zA-Z].* test")
	tname=substr($0, RSTART, RLENGTH)
	gsub(" ", "_", tname)
}

/^.* test .*PASS/ {
	match($0, "^.* test")
	tname=substr($0, RSTART, RLENGTH)
	gsub(" ", "_", tname)
        printf("%s: Pass\n", tname)
	tname=""
}

/^.* test .*FAIL/ {
	match($0, "^.* test")
	tname=substr($0, RSTART, RLENGTH)
	gsub(" ", "_", tname)
        printf("%s: Fail\n", tname)
	tname=""
}

/^.* test .*SKIP/ {
	match($0, "^.* test")
	tname=substr($0, RSTART, RLENGTH)
	gsub(" ", "_", tname)
        printf("%s: Skip\n", tname)
	tname=""
}

/^.* test .*WARNING/ {
	match($0, "^.* test")
	tname=substr($0, RSTART, RLENGTH)
	gsub(" ", "_", tname)
        printf("%s: Warning\n", tname)
	tname=""
}

/"test_\$T\/assert" command failed/ {
	if(tname != "") {
		printf("%s: Fail\n", tname)
		tname=""
	}
}
