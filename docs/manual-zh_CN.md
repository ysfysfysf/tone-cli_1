# 使用手册

## 安装

```bash
git clone https://gitee.com/anolis/tone-cli.git
cd tone-cli
make install
```

## 下载测试套件

```bash
tone fetch unixbench
```

## 安装测试套件

```bash
tone install unixbench
```

## 查看测试套件

```bash
tone list unixbench
```

## 运行测试套件

### 全量运行

```bash
tone run unixbench
```

### 按测试集运行

```bash
tone run unixbench:whetstone-double-1-10s # 使用名称索引
tone run unixbench::0 # 使用数字索引
```

## 查看结果

位置：../tone-cli/result/测试套件/测试集/运行次数/

以unixbench为例：

```bash
cd  tone-cli/result/unixbench/shell1-1-30/1/
tree -L 2
|-- env.sh        # 运行时的环境变量
|-- result.json   # 结果文件
|-- stderr.log    # 标准错误输出日志
|-- stdout.log    # 标准输入日志 
|-- sysinfo 
|   |-- post      # 运行后系统信息
|   `-- pre       # 运行前系统信息
```

## 问题及解决方法

1.在部分python2与python3共存的环境中，可能会提示：
```bash
/usr/bin/env: 'python': No such file or directory
```
可以通过添加python2或python3的软链接到python来解决这个问题

查看当前环境的python版本信息：
```bash
whereis python
```
根据当前环境的python版本做软链接的映射，以python3.6和python2.7为例：

python3（推荐）：
```bash
ln -s /usr/bin/python3.6 /usr/bin/python
```

python2：
```bash
ln -s /usr/bin/python2.7 /usr/bin/python
```
此外，pip的类似问题也可以通过上述方法来处理