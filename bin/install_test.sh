#!/bin/bash

if [ -z "$1" ]; then
    cat <<EOF
Usage:
    $0 testsuite
EOF
    exit 1
fi

testsuite=$1

[ -n "$TONE_ROOT" ] || TONE_ROOT=$(dirname $(dirname `readlink -f $0`))

. $TONE_ROOT/lib/common.sh
security_setting


. $TONE_BM_SUITE_DIR/install.sh

check_and_create_path $TONE_BM_BUILD_DIR
check_and_create_path $TONE_BM_RUN_DIR

cd $TONE_BM_BUILD_DIR
install_pkg $DEP_PKG_LIST
extract_src
build
install
