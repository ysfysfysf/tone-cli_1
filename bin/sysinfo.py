#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import subprocess


def thread_iterators():
    """
    输入：shell命令lscpu
    输出：str,以空格分隔的每个node上第一个core的位置，3/4逻辑cpu数，逻辑cpu数
    """
    cmd = "lscpu"
    output = subprocess.check_output(cmd).decode()
    threads = 1
    nodes = 1
    for line in output.split("\n"):
        (key, value) = line.split(':')
        value = value.strip()
        if key == 'Thread(s) per core':
            threads = int(value)
        elif key == 'CPU(s)':
            cpus = int(value)
        elif key == 'NUMA node(s)':
            nodes = int(value)
            break
    core_per_node = int(cpus / nodes / threads)
    iterate = ['1']
    for i in range(nodes):
        iterate.append(str((i + 1) * core_per_node))

    if cpus >= 4:
        iterate.append(str(int(cpus * 3 / 4)))
        iterate.append(str(cpus))
    print(" ".join(iterate))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(description='help')
    subcommand = subparser.add_parser("iterate_threads")
    subcommand.set_defaults(subcommand="iterate_threads")

    config = parser.parse_args()

    if config.subcommand == 'iterate_threads':
        thread_iterators()
