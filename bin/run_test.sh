#!/bin/bash

if [ -z "$1" ]; then
    cat <<EOF
Usage:
    $0 testsuite
EOF
    exit 1
fi

testsuite=$1
[ -n "$TONE_ROOT" ] || TONE_ROOT=$(dirname $(dirname `readlink -f $0`))

. $TONE_ROOT/lib/common.sh

if [ -e ./var.sh ]; then
    . ./var.sh
elif [ -e $TONE_CURRENT_RESULT_DIR/var.sh ]; then
    . $TONE_CURRENT_RESULT_DIR/var.sh
else
    echo "[tone]Error: no test suit setting (./var.sh)"
    exit 1
fi

. $TONE_BM_SUITE_DIR/run.sh

cd $TONE_BM_RUN_DIR
pwd
tone_runtest_ret=0
setup || { ((tone_runtest_ret++));echo "[tone]Error: The return code of setup() in run.sh is not 0"; }
run || { ((tone_runtest_ret++));echo "[tone]Error: The return code of run() in run.sh is not 0"; }
teardown || { ((tone_runtest_ret++));echo "[tone]Error: The return code of teardown() in run.sh is not 0"; }
exit $tone_runtest_ret
