install:
	result=$(uname -a | grep "kylin");\
	if [ x"${result}" != x"" ];then \
    		if [ -f /usr/bin/python ];then \
    			mkdir /usr/bin/python-backup/;\
    			mv -n /usr/bin/python /usr/bin/python-backup/python;\
    		fi;\
	fi;\

	if [ -h /usr/local/bin/tone ]; then \
		rm -rf /usr/local/bin/tone; \
	fi;\
	ln -s `pwd`/tone /usr/local/bin/tone;\

	if [ ! -f /usr/bin/python ]; then \
		if [ -f /usr/bin/python3 ]; then \
			ln -s /usr/bin/python3 /usr/bin/python; \
		elif [ -f /usr/bin/python2 ]; then \
			ln -s /usr/bin/python2 /usr/bin/python; \
		else \
			echo "Can Not find python in /usr/bin"; \
			exit 1; \
		fi; \
	fi
	./bin/install_tone_deps.py
	#create link of /usr/bin/python again, to workaround issue:
	#link of python would be deleted by python-devel installation
	if [ ! -f /usr/bin/python ]; then \
		if [ -f /usr/bin/python3 ]; then \
			ln -s /usr/bin/python3 /usr/bin/python; \
		elif [ -f /usr/bin/python2 ]; then \
			ln -s /usr/bin/python2 /usr/bin/python; \
		else \
			echo "Can Not find python in /usr/bin"; \
			exit 1; \
		fi; \
	fi
	echo "Done!"
clean:
	find . -type f -name "*.pyc" | xargs rm -f
	[ ! -e "./build" ] || rm -rf ./build
	[ ! -e "./cache" ] || rm -rf ./cache
	[ ! -e "./run" ] || rm -rf ./run
	[ ! -e "./result" ] || rm -rf ./result
